## violet-user 10 QKQ1.190915.002 V12.5.1.0.QFHINXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: violet
- Brand: Xiaomi
- Flavor: lineage_violet-userdebug
- Release Version: 13
- Kernel Version: 4.14.325
- Id: TQ3A.230901.001
- Incremental: 3d141fb2fc
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: xiaomi/violet/violet:10/QKQ1.190915.002/V12.5.1.0.QFHINXM:user/release-keys
- OTA version: 
- Branch: violet-user-10-QKQ1.190915.002-V12.5.1.0.QFHINXM-release-keys
- Repo: xiaomi_violet_dump_10651
